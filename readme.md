# Group Details

Vivek Batra (C0741344)  
Pintu Jat (C0744398)

---

For this Activity following Softwares were used:
Android Studio
Java
Particle

## Steps to install the game
1 Download our repository
2 Open Android Studio and open the downloaded project in it
3 Make sure that you have your own particle id and login credentials in the android main activity code.
4 Also, your particle should have the .ino file flashed on it and should be online

# Now let's run the application.
1 click on message button and say "Turn Lights Green"  
2 click on message button and say "Turn Lights Red"

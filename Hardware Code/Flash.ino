#include <InternetButton.h>

// This #include statement was automatically added by the Particle IDE.

InternetButton button = InternetButton();

void setup()
{
button.begin();
Particle.function("changeColor", colorify);
}

void loop()
{

}


int colorify(String cmd)
{
    if(cmd == "green")
    {
        button.allLedsOn(0, 255, 0);
        return 1;
    }
    else if(cmd == "red")
    {
        button.allLedsOn(255, 0, 0);
        return 1;
    }
    else
    {
    Particle.publish("Incorrect Value","green or red", 60, PRIVATE);
         return -1;
    }

}

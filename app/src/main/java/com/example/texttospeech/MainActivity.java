package com.example.texttospeech;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class MainActivity extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 1;

    private TextToSpeech TTS;
    private SpeechRecognizer speechRec;
    private String TAG = "TTS";



    private final String PARTICLE_USERNAME = "vivekbatra456@gmail.com";
    private final String PARTICLE_PASSWORD = "mightbeiamwrong";

    // MARK: Particle device-specific info
    private final String DEVICE_ID = "220026000f47363333343437";

    // MARK: Particle Publish / Subscribe variables
    private long subscriptionId;

    // MARK: Particle device
    private ParticleDevice mDevice;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // 1. Initialize your connection to the Particle API
        ParticleCloudSDK.init(this.getApplicationContext());

        // 2. Setup your device variable
        getDeviceFromCloud();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.RECORD_AUDIO)
                        != PackageManager.PERMISSION_GRANTED) {

                    // Permission is not granted
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                            Manifest.permission.RECORD_AUDIO)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    } else {
                        // No explanation needed; request the permission
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.RECORD_AUDIO},MY_PERMISSIONS_REQUEST_RECORD_AUDIO);

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                } else {
                    // Permission has already been granted
                    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                    intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS,1);
                    speechRec.startListening(intent);
                }
            }
        });
        
        initilizedTTS();
        initilizeSpeechRecognizer();
    }




    /**
     * Custom function to connect to the Particle Cloud and get the device
     */
    public void getDeviceFromCloud() {
        // This function runs in the background
        // It tries to connect to the Particle Cloud and get your device
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
                mDevice = particleCloud.getDevice(DEVICE_ID);
                return -1;

            }

            @Override
            public void onSuccess(Object o) {

                Log.d(TAG, "Successfully got device from Cloud");

            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }





    private void initilizedTTS() {
        TTS = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(TTS.getEngines().size() == 0)
                {
                    Log.d(TAG, "No TTS found");
                    finish();
                }
                else
                {
                    TTS.setLanguage(Locale.CANADA);
                    speak("Hi Vivek and Pintu");
                }
            }
        });
    }

    private void initilizeSpeechRecognizer()
    {
        if (SpeechRecognizer.isRecognitionAvailable(this))
        {
            speechRec = SpeechRecognizer.createSpeechRecognizer(this);
            speechRec.setRecognitionListener(new RecognitionListener() {
                @Override
                public void onReadyForSpeech(Bundle params) {

                }

                @Override
                public void onBeginningOfSpeech() {

                }

                @Override
                public void onRmsChanged(float rmsdB) {

                }

                @Override
                public void onBufferReceived(byte[] buffer) {

                }

                @Override
                public void onEndOfSpeech() {

                }

                @Override
                public void onError(int error) {

                }

                @Override
                public void onResults(Bundle bun) {
                    List<String> res = bun.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                    processResult(res.get(0));

                }

                @Override
                public void onPartialResults(Bundle partialResults) {

                }

                @Override
                public void onEvent(int eventType, Bundle params) {

                }
            });
        }
    }

    private void processResult(String s) {
        s = s.toLowerCase();
        if (s.indexOf("turn lights green") != -1)
        {
            speak("i am turning lights green");



            Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
                @Override
                public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                    // put your logic here to talk to the particle
                    // --------------------------------------------

                    // what functions are "public" on the particle?
                    Log.d(TAG, "Availble functions: " + mDevice.getFunctions());


                    List<String> functionParameters = new ArrayList<String>();
                    functionParameters.add("green");
                    try {
                        mDevice.callFunction("changeColor", functionParameters);

                    } catch (ParticleDevice.FunctionDoesNotExistException e1) {
                        e1.printStackTrace();
                    }


                    return -1;
                }

                @Override
                public void onSuccess(Object o) {
                    // put your success message here
                    Log.d(TAG, "Success!");
                }

                @Override
                public void onFailure(ParticleCloudException exception) {
                    // put your error handling code here
                    Log.d(TAG, exception.getBestMessage());
                }
            });

        }
        else if (s.indexOf("turn lights red") != -1)
        {
            speak("i am turning lights red");

            Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
                @Override
                public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                    // put your logic here to talk to the particle
                    // --------------------------------------------

                    // what functions are "public" on the particle?
                    Log.d(TAG, "Availble functions: " + mDevice.getFunctions());


                    List<String> functionParameters = new ArrayList<String>();
                    functionParameters.add("red");
                    try {
                        mDevice.callFunction("changeColor", functionParameters);

                    } catch (ParticleDevice.FunctionDoesNotExistException e1) {
                        e1.printStackTrace();
                    }


                    return -1;
                }

                @Override
                public void onSuccess(Object o) {
                    // put your success message here
                    Log.d(TAG, "Success!");
                }

                @Override
                public void onFailure(ParticleCloudException exception) {
                    // put your error handling code here
                    Log.d(TAG, exception.getBestMessage());
                }
            });
        }
        else
        {
            speak("I did not get you");
        }
    }


    private void speak(String message)
    {

        if (Build.VERSION.SDK_INT >= 21)
        {
            TTS.speak(message, TextToSpeech.QUEUE_FLUSH, null, null);
        }
        else
        {
            TTS.speak(message, TextToSpeech.QUEUE_FLUSH, null);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPause() {
        super.onPause();
        TTS.shutdown();
    }

    public void listenNow(View view)
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        speechRec.startListening(intent);
    }




}
